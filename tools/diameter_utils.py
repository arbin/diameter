__program_name__ = "DIAMETER Utils"
__about__ = "Utilities for desktop applications."
__author__ = "Arbin Bulaybulay"

import datetime
import json
import os
import psutil
import requests

from requests.auth import HTTPBasicAuth


class DiskInfo:
    def __init__(self, config, method):
        self.disk_usage = psutil.disk_usage(config['archived_disk'])
        self.project_id = config['project_id']
        self.project_name = config['project_name']
        self.username = config['auth']['username']
        self.password = config['auth']['password']
        self.create_url = config['url']['create']
        self.update_url = config['url']['update']
        self.headers = config['headers']

    def get_disk_info(self):
        cpu_percent = psutil.cpu_percent(interval=1)  # CPU performance
        memory = psutil.virtual_memory()
        memory = memory.available  # Available memory
        disk = psutil.disk_io_counters()
        disk = disk.read_bytes
        return {"cpu_percent": cpu_percent, "memory": memory, "disk": disk}

    def get_disk_utilization(self):
        percent_used = self.disk_usage.percent
        used_space = self.disk_usage.used
        free_space = self.disk_usage.free
        return {"percent_used": percent_used,
            "used_space": used_space,
            "free_space": free_space}

    def post_disk_utilization(self):
        info = self.get_disk_utilization()
        data = {"id": self.project_id,
            "project_name": self.project_name,
            "used_space": str(info["used_space"]),
            "free_space": str(info["free_space"]),
            "percent_used": str(info["percent_used"]),
            "last_update": str(datetime.datetime.now())}

        session = requests.Session()
        session.auth = HTTPBasicAuth(
            self.username,
            self.password
            )
        if method == 'create':
            response = session.post(
                self.create_url,
                data=json.dumps(data),
                headers=self.headers
                )
        else:
            response = session.put(
                self.update_url.replace('%s', config['project_id']),
                data=json.dumps(data),
                headers=self.headers
                )
        print((response.status_code))


def settings(json_file):
    """ Returns iterable json data from a json file """
    try:
        if os.path.isfile(json_file):
            with open(json_file) as json_settings:
                json_content = json_settings.read()
                settings_content = json.loads(json_content)
                return settings_content
        else:
            print('No settings file found')
    except Exception as e:
        print(e)


if __name__ == '__main__':
    config = settings('settings.json')
    method = config['method']
    post = DiskInfo(config, method)
    post.post_disk_utilization()