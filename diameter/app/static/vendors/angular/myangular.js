var myApp = angular.module('preachApp')

myApp.controller('homeController', function($scope) {
    $scope.message = 'I am the homepage';
});

myApp.config(function($locationProvider) {
    $locationProvider.html5Mode(true);
});

// Our myangular.js
var preachApp = angular.module('preachApp', ['ngRoute']);

preachApp.config(function($locationProvider) {
    $locationProvider.html5Mode(true);
});

preachApp.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/diamete/app/templates/app/sample.html',
            controller: 'homeController'
        })

    .otherwise({
        redirectTo: '/'
    });
});

preachApp.controller('settingsController', function($scope) {
    $scope.message = 'I am on settings page';
});