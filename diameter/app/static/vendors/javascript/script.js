app = angular.module 'example.app.basic', []

app.controller 'AppController', ['$scope', '$http', ($scope, $http) ->
    $scope.disk-performance = []
    $http.get('/api/disk-performance/').then (result) ->
        angular.forEach result.data, (item) ->
            $scope.disk-performance.push item
]