var app = angular.module('JobApp', [
     'ui.router',
     'restangular'
 ])
 
 app.config(function ($stateProvider, $urlRouterProvider, RestangularProvider) {
     // For any unmatched url, send to /route1
     $urlRouterProvider.otherwise("/");
     $stateProvider
         .state('sample', {
 
             url: "/api/disk-performance/",
             templateUrl: "/app/templates/sample.html",
             controller: "JobList"
         })
 
 })
 
 app.controller("JobFormCtrl", ['$scope', 'Restangular', 'CbgenRestangular', '$q',
 function ($scope, Restangular, CbgenRestangular, $q) {
 
 
 }])// end controller