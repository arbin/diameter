# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from django.shortcuts import render
from rest_framework import generics, permissions, status, response
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.contrib.auth.tokens import default_token_generator
from . import settings, utils
from .disk_info.models import *
from . import serializers

from django.utils import timezone

import datetime


User = get_user_model()


class RootView(generics.GenericAPIView):
    """
    Root endpoint - use one of sub endpoints.
    """

    def get(self, request, format=None):
        urls_mapping = {
            'register': 'register',
            'login': 'login',
            'forgot-password': 'forgot_password',
        }
        return Response({key: reverse(url_name, request=request, format=format)
                         for key, url_name in urls_mapping.items()})


class RegistrationView(utils.SendEmailViewMixin, generics.CreateAPIView):
    """
    Use this endpoint to register new user.
    """
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def get_serializer_class(self):
        if settings.get('LOGIN_AFTER_REGISTRATION'):
            return serializers.UserRegistrationWithAuthTokenSerializer
        return serializers.UserRegistrationSerializer

    def post_save(self, obj, created=False):
        if settings.get('LOGIN_AFTER_REGISTRATION'):
            Token.objects.get_or_create(user=obj)
        if settings.get('SEND_ACTIVATION_EMAIL'):
            self.send_email(**self.get_send_email_kwargs(obj))

    def perform_create(self, serializer):
        instance = serializer.save()
        self.post_save(obj=instance, created=True)

    def get_send_email_extras(self):
        return {
            'subject_template_name': 'activation_email_subject.html',
            'plain_body_template_name': 'activation_email_body.html',
        }

    def get_email_context(self, user):
        context = super(RegistrationView, self).get_email_context(user)
        context['url'] = settings.get('ACTIVATION_URL').format(**context)
        return context

"""
class LoginView(utils.ActionViewMixin, generics.GenericAPIView):

    # Use this endpoint to obtain user authentication token.

    serializer_class = serializers.UserLoginSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, serializer):
        token, _ = Token.objects.get_or_create(user=serializer.object)
        return Response(
            data=serializers.TokenSerializer(token).data,
            status=status.HTTP_200_OK,
        )
"""

class PasswordResetView(utils.ActionViewMixin, utils.SendEmailViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to send email to user with password reset link.
    """
    serializer_class = serializers.PasswordResetSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def action(self, serializer):
        for user in self.get_users(serializer.data['email']):
            self.send_email(**self.get_send_email_kwargs(user))
        return response.Response(status=status.HTTP_200_OK)

    def get_users(self, email):
        active_users = User._default_manager.filter(
            email__iexact=email,
            is_active=True,
        )
        return (u for u in active_users if u.has_usable_password())

    def get_send_email_extras(self):
        return {
            'subject_template_name': 'password_reset_email_subject.html',
            'plain_body_template_name': 'password_reset_email_body.html',
        }

    def get_email_context(self, user):
        context = super(PasswordResetView, self).get_email_context(user)
        context['url'] = settings.get('PASSWORD_RESET_CONFIRM_URL').format(**context)
        return context


class PasswordResetConfirmView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to finish reset password process.
    """
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def get_serializer_class(self):
        if settings.get('PASSWORD_RESET_CONFIRM_RETYPE'):
            return serializers.PasswordResetConfirmRetypeSerializer
        return serializers.PasswordResetConfirmSerializer

    def action(self, serializer):
        serializer.user.set_password(serializer.data['new_password'])
        serializer.user.save()
        return response.Response(status=status.HTTP_200_OK)


class ArchiveDiskListView(generics.ListCreateAPIView):
    queryset = ArchiveDisk.objects.all()
    serializer_class = serializers.ArchiveDiskSerializer


class ArchiveDiskView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ArchiveDiskSerializer
    queryset = ArchiveDisk.objects.all()


class DiskPerformanceListView(generics.ListCreateAPIView):
    queryset = DiskPerformance.objects.all()
    serializer_class = serializers.DiskPerformanceSerializer


class DiskPerformanceView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.DiskPerformanceSerializer
    queryset = DiskPerformance.objects.all()


class ArchivingScheduleView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ArchivingScheduleSerializer
    queryset = ArchivingSchedule.objects.all()


class ArchivingProgressListView(generics.ListCreateAPIView):
    queryset = ArchivingProgress.objects.all()
    serializer_class = serializers.ArchivingProgressSerializer


class ArchivingProgressView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ArchivingProgressSerializer
    queryset = ArchivingProgress.objects.all()


class RetentionPeriodView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.RetentionPeriodSerializer
    queryset = RetentionPeriod.objects.all()


class SourceDestinationView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.SourceDestinationSerializer
    queryset = SourceDestination.objects.all()


class StagedArchivesListView(generics.ListCreateAPIView):
    queryset = StagedArchives.objects.all()
    serializer_class = serializers.StagedArchivesSerializer


class StagedArchivesView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.StagedArchivesSerializer
    queryset = StagedArchives.objects.all()


class StagedArchivesListOnlyView(generics.ListAPIView):
    serializer_class = serializers.StagedArchivesListSerializer
    queryset = StagedArchives.objects.all()