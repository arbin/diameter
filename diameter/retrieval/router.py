class DB_Brain(object):
    def db_for_read(self, model, **hints):
        ""
        if model._meta.app_label == 'retrieval':
            return 'dbbrain'
        return 'default'

    def db_for_write(self, model, **hints):
        ""
        if model._meta.app_label == 'retrieval':
            return 'dbbrain'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        ""
        if obj1._meta.app_label == 'retrieval' and obj2._meta.app_label == 'retrieval':
            return True
        elif 'retrieval' not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_syncdb(self, db, model):
        if db == 'dbbrain' or model._meta.app_label == "retrieval":
            return False # we're not using syncdb on our legacy database
        else: # but all other models/databases are fine
            return True