# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class TblEsburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=11, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    issuename = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=30, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)
    resupplynum = models.IntegerField(db_column='resupplyNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tbl_esburnlog'
        app_label = 'retrieval'
        verbose_name = "Elsevier Archive"


class TblIeeeburnlog(models.Model):
    count = models.IntegerField(blank=True, null=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    datefoldername = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=20, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_ieeeburnlog'
        app_label = 'retrieval'
        verbose_name = "IEEE Archive"


class TblIrnpdburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    project = models.CharField(max_length=20, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    origpath = models.TextField(blank=True, null=True)
    disklabel = models.CharField(max_length=20, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_irnpdburnlog'
        app_label = 'dbbrain'


class TblLwwburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=80, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=20, blank=True, null=True)
    issuename = models.CharField(max_length=20, blank=True, null=True)
    disklabel = models.CharField(max_length=40, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_lwwburnlog'
        app_label = 'retrieval'
        verbose_name = "LWW Archive"


class TblRawfilesburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    origpath = models.TextField(blank=True, null=True)
    disklabel = models.CharField(max_length=20, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_rawfilesburnlog'
        app_label = 'retrieval'


class TblSpringerburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=10, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    existfolder = models.TextField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    issuename = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=60, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_springerburnlog'
        app_label = 'retrieval'
        verbose_name = "Springer Archive"


class TblWbburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=10, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    existfolder = models.TextField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    issuename = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=60, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_wbburnlog'
        app_label = 'retrieval'
        verbose_name = "WB Archive"