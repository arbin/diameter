from django.contrib import admin

# Register your models here.
from models import *


class TblEsburnlogAdmin(admin.ModelAdmin):
    list_display = ('articlename', 'dateburned', 'journalcode',
                    'timestamp', 'disklabel')
    search_fields = ('articlename', 'disklabel')


class TblSpringerburnlogAdmin(admin.ModelAdmin):
    list_display = ('articlename', 'issuename', 'dateburned', 'journalcode',
                    'timestamp', 'disklabel')
    search_fields = ('articlename', 'disklabel', 'issuename')

admin.site.register(TblEsburnlog, TblEsburnlogAdmin)
admin.site.register(TblSpringerburnlog, TblSpringerburnlogAdmin)