# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class TblErrorlog(models.Model):
    count = models.AutoField(primary_key=True)
    date = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    stage = models.CharField(max_length=10, blank=True, null=True)
    project = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_errorlog'
        app_label = 'retrieval'


class TblEsburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=11, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    issuename = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=30, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)
    resupplynum = models.IntegerField(db_column='resupplyNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tbl_esburnlog'
        app_label = 'retrieval'
        verbose_name = "Elsevier Archive"

"""
class TblForburnitems(models.Model):
    record_id = models.AutoField(primary_key=True)
    foldername = models.CharField(db_column='folderName', max_length=15)  # Field name made lowercase.
    folderpath = models.CharField(db_column='folderPath', max_length=150)  # Field name made lowercase.
    ipaddress = models.CharField(db_column='IPaddress', max_length=30)  # Field name made lowercase.
    activityid = models.CharField(db_column='activityID', max_length=40)  # Field name made lowercase.
    processed = models.IntegerField()
    status = models.IntegerField()
    dateadded = models.DateTimeField()
    dateprocessed = models.DateTimeField()
    projectname = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'tbl_forburnitems'
        app_label = 'retrieval'
        verbose_name = "Elsevier Archive"
"""

class TblIeeeburnlog(models.Model):
    count = models.IntegerField(blank=True, null=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    datefoldername = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=20, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_ieeeburnlog'
        app_label = 'retrieval'
        verbose_name = "IEEE Archive"


class TblIrnpdburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    project = models.CharField(max_length=20, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    origpath = models.TextField(blank=True, null=True)
    disklabel = models.CharField(max_length=20, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_irnpdburnlog'
        app_label = 'dbbrain'


class TblLwwburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=80, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=20, blank=True, null=True)
    issuename = models.CharField(max_length=20, blank=True, null=True)
    disklabel = models.CharField(max_length=40, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_lwwburnlog'
        app_label = 'retrieval'
        verbose_name = "LWW Archive"


class TblRawfilesburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=8, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    origpath = models.TextField(blank=True, null=True)
    disklabel = models.CharField(max_length=20, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_rawfilesburnlog'
        app_label = 'dbbrain'

""""
class TblRequests(models.Model):
    cnt = models.AutoField(primary_key=True)
    jobname = models.CharField(max_length=20, blank=True, null=True)
    project = models.CharField(max_length=20, blank=True, null=True)
    stage = models.CharField(max_length=20, blank=True, null=True)
    requestedby = models.CharField(max_length=80, blank=True, null=True)
    requesteddate = models.DateTimeField(blank=True, null=True)
    restoredby = models.CharField(max_length=80, blank=True, null=True)
    restoreddate = models.DateTimeField(blank=True, null=True)
    restoredpath = models.CharField(db_column='restoredPath', max_length=160, blank=True, null=True)  # Field name made lowercase.
    requestreason = models.CharField(max_length=120, blank=True, null=True)
    priority = models.CharField(max_length=16, blank=True, null=True)
    done = models.CharField(max_length=1, blank=True, null=True)
    process = models.CharField(max_length=1, blank=True, null=True)
    ticketnumber = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_requests'
        app_label = 'dbbrain'
"""

class TblSpringerburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=10, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    existfolder = models.TextField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    issuename = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=60, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_springerburnlog'
        app_label = 'retrieval'
        verbose_name = "Springer Archive"


class TblWbburnlog(models.Model):
    count = models.AutoField(primary_key=True)
    dateburned = models.DateTimeField(blank=True, null=True)
    journalcode = models.CharField(db_column='journalCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    articlename = models.CharField(max_length=10, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    existfolder = models.TextField(blank=True, null=True)
    stage = models.CharField(max_length=6, blank=True, null=True)
    issuename = models.CharField(max_length=12, blank=True, null=True)
    disklabel = models.CharField(max_length=60, blank=True, null=True)
    archivedby = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_wbburnlog'
        app_label = 'retrieval'
        verbose_name = "WB Archive"


class TblburnlogOld(models.Model):
    dateburned = models.DateTimeField(blank=True, null=True)
    articlename = models.CharField(max_length=8, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    stage = models.CharField(max_length=10, blank=True, null=True)
    disklabel = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tblburnlog_old'
        app_label = 'dbbrain'


class Tblusers(models.Model):
    emp_id = models.CharField(primary_key=True, max_length=4)
    emp_name = models.CharField(max_length=20, blank=True, null=True)
    position = models.CharField(max_length=15, blank=True, null=True)
    access_level = models.CharField(max_length=1, blank=True, null=True)
    username = models.CharField(max_length=20, blank=True, null=True)
    password = models.CharField(max_length=40, blank=True, null=True)
    email = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tblusers'
        app_label = 'dbbrain'
