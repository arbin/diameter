from distutils import version
from django.contrib.auth import authenticate, get_user_model
from .disk_info.models import *
from rest_framework import serializers
import rest_framework
from rest_framework.authtoken.models import Token
from . import constants, utils

User = get_user_model()


def create_username_field():
    username_field = User._meta.get_field(User.USERNAME_FIELD)
    if hasattr(serializers.ModelSerializer, 'field_mapping'):
        mapping_dict = serializers.ModelSerializer.field_mapping
    else:
        mapping_dict = serializers.ModelSerializer._field_mapping.mapping
    field_class = mapping_dict[username_field.__class__]
    return field_class()


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            User._meta.pk.name,
            User.USERNAME_FIELD,
        )
        read_only_fields = (
            User.USERNAME_FIELD,
        )


class AbstractUserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            User.USERNAME_FIELD,
            User._meta.pk.name,
            'password',
        )
        write_only_fields = (
            'password',
        )

if version.StrictVersion(rest_framework.VERSION) >= version.StrictVersion('3.0.0'):

    class UserRegistrationSerializer(AbstractUserRegistrationSerializer):

        def create(self, validated_data):
            return User.objects.create_user(**validated_data)

else:

    class UserRegistrationSerializer(AbstractUserRegistrationSerializer):

        def restore_object(self, attrs, instance=None):
            try:
                return User.objects.get(**{User.USERNAME_FIELD: attrs[User.USERNAME_FIELD]})
            except User.DoesNotExist:
                return User.objects.create_user(**attrs)

        def save_object(self, obj, **kwargs):
            return obj


class UserRegistrationWithAuthTokenSerializer(UserRegistrationSerializer):
    auth_token = serializers.SerializerMethodField(method_name='get_user_auth_token')

    class Meta(UserRegistrationSerializer.Meta):
        model = User
        fields = UserRegistrationSerializer.Meta.fields + (
            'auth_token',
        )

    def get_user_auth_token(self, obj):
        return obj.auth_token.key


class UserLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'username',
            'password',
        )
        #write_only_fields = (
            #'password',

        #)

    def __init__(self, *args, **kwargs):
        super(UserLoginSerializer, self).__init__(*args, **kwargs)
        self.fields[User.USERNAME_FIELD] = create_username_field()

    def validate(self, attrs):
        self.object = authenticate(username=attrs[User.USERNAME_FIELD], password=attrs['password'])
        if self.object:
            if not self.object.is_active:
                raise serializers.ValidationError(constants.DISABLE_ACCOUNT_ERROR)
            return attrs
        else:
            raise serializers.ValidationError(constants.INVALID_CREDENTIALS_ERROR)


class UserLogoutSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'username',
            'password',
        )
        write_only_fields = (
            'username',
            'password',
        )

    def __init__(self, *args, **kwargs):
        super(UserLogoutSerializer, self).__init__(*args, **kwargs)
        self.fields[User.USERNAME_FIELD] = create_username_field()


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()


class UidAndTokenSerializer(serializers.Serializer):
    uid = serializers.CharField()
    token = serializers.CharField()

    def validate_uid(self, attrs_or_value, source=None):
        value = attrs_or_value[source] if source else attrs_or_value
        try:
            uid = utils.decode_uid(value)
            self.user = User.objects.get(pk=uid)
        except (User.DoesNotExist, ValueError, TypeError, ValueError, OverflowError) as error:
            raise serializers.ValidationError(error)
        return attrs_or_value

    def validate(self, attrs):
        attrs = super(UidAndTokenSerializer, self).validate(attrs)
        if not self.context['view'].token_generator.check_token(self.user, attrs['token']):
            raise serializers.ValidationError(constants.INVALID_TOKEN_ERROR)
        return attrs


class PasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField()


class PasswordRetypeSerializer(PasswordSerializer):
    re_new_password = serializers.CharField()

    def validate(self, attrs):
        attrs = super(PasswordRetypeSerializer, self).validate(attrs)
        if attrs['new_password'] != attrs['re_new_password']:
            raise serializers.ValidationError(constants.PASSWORD_MISMATCH_ERROR)
        return attrs


class CurrentPasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField()

    def validate_current_password(self, attrs_or_value, source=None):
        value = attrs_or_value[source] if source else attrs_or_value
        if not self.context['request'].user.check_password(value):
            raise serializers.ValidationError(constants.INVALID_PASSWORD_ERROR)
        return attrs_or_value


class SetPasswordSerializer(PasswordSerializer, CurrentPasswordSerializer):
    pass


class SetPasswordRetypeSerializer(PasswordRetypeSerializer, CurrentPasswordSerializer):
    pass


class PasswordResetConfirmSerializer(UidAndTokenSerializer, PasswordSerializer):
    pass


class PasswordResetConfirmRetypeSerializer(UidAndTokenSerializer, PasswordRetypeSerializer):
    pass


class SetUsernameSerializer(CurrentPasswordSerializer):

    def __init__(self, *args, **kwargs):
        super(SetUsernameSerializer, self).__init__(*args, **kwargs)
        self.fields['new_' + User.USERNAME_FIELD] = create_username_field()


class SetUsernameRetypeSerializer(SetUsernameSerializer):

    def __init__(self, *args, **kwargs):
        super(SetUsernameRetypeSerializer, self).__init__(*args, **kwargs)
        self.fields['re_new_' + User.USERNAME_FIELD] = create_username_field()

    def validate(self, attrs):
        attrs = super(SetUsernameRetypeSerializer, self).validate(attrs)
        if attrs['new_' + User.USERNAME_FIELD] != attrs['re_new_' + User.USERNAME_FIELD]:
            raise serializers.ValidationError(constants.USERNAME_MISMATCH_ERROR.format(User.USERNAME_FIELD))
        return attrs


class TokenSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(source='key')

    class Meta:
        model = Token
        fields = (
            'auth_token',
        )


class ArchiveDiskSerializer(serializers.ModelSerializer):

    class Meta:
        model = ArchiveDisk
        fields = ('id', 'project_name', 'used_space', 'free_space', 'percent_used', 'last_update')

    def create(self, validated_data):
        return ArchiveDisk.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.used_space = validated_data.get('used_space', instance.used_space)
        instance.free_space = validated_data.get('free_space', instance.free_space)
        instance.percent_used = validated_data.get('percent_used', instance.percent_used)
        instance.last_update = validated_data.get('last_update', instance.last_update)
        instance.save()
        return instance


class DiskPerformanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = DiskPerformance
        fields = ('id', 'project_name', 'cpu', 'memory', 'disk', 'last_update')


class archivedisk_diskperformanceSerializer(serializers.ModelSerializer):
    archivedisk_diskperformance = ArchiveDiskSerializer(many=True)

    class Meta:
        model = DiskPerformance
        fields = ('id', 'project_name', 'cpu', 'memory', 'disk', 'last_update', 'archivedisk_diskperformance')

    def create(self, validated_data):
        archivedisks_data = validated_data.pop('archivedisk_diskperformance')
        diskperformance = DiskPerformance.objects.create(**validated_data)
        for archivedisk_data in archivedisks_data:
            ArchiveDisk.objects.create(project_name=diskperformance, **archivedisk_data)
        return diskperformance

    def update(self, instance, validated_data):
        archivedisks_data = validated_data.pop('archivedisk_diskperformance')
        archivedisks = (instance.archivedisk_diskperformanc).all()
        archivedisks = list(archivedisks)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.cpu = validated_data.get('cpu', instance.cpu)
        instance.memory = validated_data.get('memory', instance.memory)
        instance.disk = validated_data.get('disk', instance.disk)
        instance.last_update = validated_data.get('last_update', instance.last_update)
        instance.save()

        for archivedisk_data in archivedisks_data:
            archivedisk = archivedisks.pop(0)
            archivedisk.project_name = archivedisk_data.get('project_name', archivedisk.project_name)
            archivedisk.used_space = archivedisk_data.get('used_space', archivedisk.used_space)
            archivedisk.free_space = archivedisk_data.get('free_space', archivedisk.free_space)
            archivedisk.last_update = archivedisk_data.get('last_update', archivedisk.last_update)
            archivedisk.save()
        return instance


class ArchivingScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = ArchivingSchedule
        fields = ('id', 'project_name', 'stage_name', 'schedule')

    def create(self, validated_data):
        return ArchivingSchedule.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.stage_name = validated_data.get('stage_name', instance.stage_name)
        instance.schedule = validated_data.get('schedule', instance.schedule)
        instance.save()
        return instance


class ArchivingProgressSerializer(serializers.ModelSerializer):

    class Meta:
        model = ArchivingProgress
        fields = ('id', 'project_name', 'stage_name', 'progress', 'date_done')

    def create(self, validated_data):
        return ArchivingProgress.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.stage_name = validated_data.get('stage_name', instance.stage_name)
        instance.progess = validated_data.get('progress', instance.progress)
        instance.date_done = validated_data.get('date_done', instance.date_done)
        instance.save()
        return instance


class RetentionPeriodSerializer(serializers.ModelSerializer):

    class Meta:
        model = RetentionPeriod
        fields = ('id', 'project_name', 'stage_name', 'schedule')

    def create(self, validated_data):
        return RetentionPeriod.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.stage_name = validated_data.get('stage_name', instance.stage_name)
        instance.schedule = validated_data.get('schedule', instance.schedule)
        instance.save()
        return instance


class SourceDestinationSerializer(serializers.ModelSerializer):

    class Meta:
        model = SourceDestination
        fields = ('id', 'project_name', 'source_disk', 'destination_disk')

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.source_disk = validated_data.get('source_disk', instance.source_disk)
        instance.destination_disk = validated_data.get('destination_disk', instance.destination_disk)
        instance.save()
        return instance


class StagedArchivesSerializer(serializers.ModelSerializer):

    class Meta:
        model = StagedArchives
        fields = ('id', 'project_name', 'file_name', 'stage_name', 'size', 'location', 'date_added')

    def create(self, validated_data):
        return StagedArchives.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.project_name = validated_data.get('project_name', instance.project_name)
        instance.file_name = validated_data.get('used_space', instance.file_name)
        instance.stage_name = validated_data.get('free_space', instance.stage_name)
        instance.size = validated_data.get('percent_used', instance.size)
        instance.date_added = validated_data.get('last_update', instance.date_added)
        instance.save()
        return instance


class StagedArchivesListSerializer(serializers.ModelSerializer):

    class Meta:
        model = StagedArchives
        fields = ('id', 'project_name', 'file_name', 'stage_name', 'size', 'location', 'date_added')

    def create(self, validated_data):
        return StagedArchives.objects.create(**validated_data)