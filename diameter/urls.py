"""diameter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
#from django_sysinfo.views import http_basic_login, sysinfo, version
from .app import views as app
from . import views
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Diameter API')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('^$', app.index),
    url('^sample/', app.sample, name='sample'),
    url('^.*html$', app.gentella_html),
    url('login/', app.login),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/register$', views.RegistrationView.as_view(), name='register'),
    url(r'^api/disk-info/create$', views.ArchiveDiskListView.as_view(), name='archive_disk'),
    url(r'^api/disk-info/update/(?P<pk>\d+)$', views.ArchiveDiskView.as_view()),
    url(r'^api/disk-performance/$', views.DiskPerformanceListView.as_view()),
    url(r'^api/disk-performance/(?P<pk>\d+)/$', views.DiskPerformanceView.as_view()),
    url(r'^api/archiving-schedule/update/(?P<pk>\d+)$', views.ArchivingScheduleView.as_view()),
    url(r'^api/progress/create$', views.ArchivingProgressListView.as_view(), name='archive_disk'),
    url(r'^api/progress/update/(?P<pk>\d+)$', views.ArchivingProgressView.as_view()),
    url(r'^api/retention-period/update/(?P<pk>\d+)$', views.RetentionPeriodView.as_view()),
    url(r'^api/source-destination/(?P<pk>\d+)$', views.RetentionPeriodView.as_view()),
    url(r'^api/staging/create$', views.StagedArchivesListView.as_view(), name='archive_disk'),
    url(r'^api/staging/update/(?P<pk>\d+)$', views.StagedArchivesView.as_view()),
    url(r'^api/staging/list/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/$', views.StagedArchivesListOnlyView.as_view()),
    url(r'^api$', schema_view),
]
