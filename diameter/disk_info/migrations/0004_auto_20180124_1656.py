# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-01-24 08:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('disk_info', '0003_auto_20180124_1653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archivedisk',
            name='free_space',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='archivedisk',
            name='used_space',
            field=models.BigIntegerField(),
        ),
    ]
