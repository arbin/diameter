# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-02-08 06:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('disk_info', '0012_auto_20180208_1406'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stagedarchives',
            name='location',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
