from __future__ import unicode_literals

from django.apps import AppConfig


class DiskInfoConfig(AppConfig):
    name = 'disk_info'
