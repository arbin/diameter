# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.


class ArchiveDisk(models.Model):
    project_name = models.CharField(max_length=100)
    used_space = models.BigIntegerField()
    free_space = models.BigIntegerField()
    percent_used = models.FloatField(blank=True, null=True)
    last_update = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.project_name


class DiskPerformance(models.Model):
    project_name = models.ForeignKey(ArchiveDisk, on_delete=models.CASCADE, related_name='archivedisk_diskperformance', null=True, blank=True)
    cpu = models.BigIntegerField()
    memory = models.BigIntegerField()
    disk = models.BigIntegerField()
    last_update = models.DateTimeField(default=timezone.now)


class ArchivingSchedule(models.Model):
    project_name = models.ForeignKey(ArchiveDisk, on_delete=models.CASCADE, related_name='archivedisk_archivingschedule', null=True, blank=True)
    stage_name = models.CharField(max_length=40)
    schedule = models.DateTimeField()

    class Meta:
        verbose_name = "Archiving Schedule"


class ArchivingProgress(models.Model):
    project_name = models.ForeignKey(ArchiveDisk, on_delete=models.CASCADE, related_name='archivedisk_archivingprogress', null=True, blank=True)
    stage_name = models.CharField(max_length=40)
    progress = models.FloatField(blank=True, null=True)
    date_done = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = "Archiving Progress"


class RetentionPeriod(models.Model):
    project_name = models.ForeignKey(ArchiveDisk, on_delete=models.CASCADE, related_name='archivedisk_retentionperiod', null=True, blank=True)
    stage_name = models.CharField(max_length=40)
    schedule = models.IntegerField()

    class Meta:
        verbose_name = "Retention Period"


class StagedArchives(models.Model):
    project_name = models.ForeignKey(ArchiveDisk, on_delete=models.CASCADE, related_name='archivedisk_stagedarchives', null=True, blank=True)
    file_name = models.CharField(max_length=40)
    stage_name = models.CharField(max_length=40)
    size = models.IntegerField()
    location = models.CharField(max_length=100, null=True)
    date_added = models.DateTimeField()

    class Meta:
        verbose_name = "Staged Archives"


class SourceDestination(models.Model):
    project_name = models.ForeignKey(ArchiveDisk, on_delete=models.CASCADE, related_name='archivedisk_sourcedestination', null=True, blank=True)
    source_disk = models.CharField(max_length=40)
    destination_disk = models.CharField(max_length=40)

    class Meta:
        verbose_name = "Source Destination"