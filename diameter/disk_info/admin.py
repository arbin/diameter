from django.contrib import admin

# Register your models here.
from models import *


class ArchiveDiskAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'used_space', 'free_space', 'last_update')
    search_fields = ('project_name',)


class DiskPerformanceAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'cpu', 'memory', 'disk', 'last_update')
    search_fields = ('project_name',)


class ArchivingScheduleAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'stage_name', 'schedule')
    search_fields = ('project_name',)


class ArchivingProgressAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'stage_name', 'progress', 'date_done')
    search_fields = ('project_name',)


class RetentionPeriodAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'stage_name', 'schedule')
    search_fields = ('project_name',)


class StagedArchivesAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'file_name', 'stage_name', 'size', 'location', 'date_added')
    search_fields = ('project_name',)


class SourceDestinationAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name', 'source_disk', 'destination_disk')
    search_fields = ('project_name',)


admin.site.register(ArchiveDisk, ArchiveDiskAdmin)
admin.site.register(DiskPerformance, DiskPerformanceAdmin)
admin.site.register(ArchivingSchedule, ArchivingScheduleAdmin)
admin.site.register(ArchivingProgress, ArchivingProgressAdmin)
admin.site.register(RetentionPeriod, RetentionPeriodAdmin)
admin.site.register(StagedArchives, StagedArchivesAdmin)
admin.site.register(SourceDestination, SourceDestinationAdmin)